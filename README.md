## LIBA: Privacy policy

Welcome to the LIBA app for Android!

This is an Android app developed by Red Pill Inc.

Considering what we do, we take privacy very seriously!
And we take as much care to keep everything safe and secure.

Highlights:
        <br/>1. Your private data is completely invisible to everyone including us!
        <br/>2. Unless you are knowingly sharing it with others, no other person can see or share this data.
        <br/>3. Photos/Images used in the app are not backed up by us, they stay in your phone. It will be your responsibility to take a back-up of all the images under "Liba Images" from time to time.
<br/><br/>
<hr style="border:1px solid gray">
This is how your data is managed:

Private data -
    <br/>Apart from your data being stored in your phone, it is backed up to Google's Firestore for recovery in case of you wanting to switch your device or in the advent of an unfortunate event like loss of device.

    We do not look at your data in any way nor do we collect stats from your private data.
    There is a possibility that algorithms internal to the app can read this data to give you suggestions. They are designed only to help you.

Public data -
<br/>Data shared with others can become public depending on how you or others you share, deal with it.

Data shared publicly can be used to generate stats and can be visible to many.

<br/>Lastly, as a user - always be mindful about what you are sharing with others.

We do not sell your data for making money.

We do not use your data to advertise in any way.

And we intend to keep it this way!

<hr style="border:1px solid gray">

If you find any security vulnerability that has been inadvertently caused by us, or have any question regarding how the app protectes your privacy, please send us an email and we will surely try to fix it/help you.

Yours sincerely,
<br/>LIBA team
<br/>dheeraj.gate@gmail.com